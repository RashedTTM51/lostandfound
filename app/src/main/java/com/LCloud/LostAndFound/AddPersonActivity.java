package com.LCloud.LostAndFound;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.DialogFragment;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.LCloud.LostAndFound.DBControl.DbHelper;
import com.LCloud.LostAndFound.DBControl.FeedEntry;
import com.LCloud.LostAndFound.Fragments.LastTimeSeenDatePicker;
import com.LCloud.LostAndFound.Model.LostAndFoundData;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class AddPersonActivity extends AppCompatActivity {
    ImageButton imgBtn;
    Button   addBtn;
    DbHelper db;
    EditText idTxt;
    EditText nameTxt;
    EditText ageTxt;
    EditText genderTxt;
    EditText raceTxt;
    EditText eyesTxt;
    EditText heightTxt;
    EditText weightTxt;
    TextView missingDateTxt;
    EditText missingLocationTxt;
    EditText moreIdCharTxt;
    EditText moreDetailsTxt;
    EditText tel1Txt;
    EditText tel2Txt;
    static final int CAMERA_REQUEST_CODE=1234191;
    static  final  int STORAGE_REQUEST_CODE =123411;

    private static final String TEMP_IMAGE_NAME = "tempImage";
    private static final String TAG = "ImagePicker";
    private void getPickImageIntent(){
        final Dialog dialog = new Dialog(AddPersonActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams WMLP = dialog.getWindow().getAttributes();
        WMLP.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(WMLP);
//        dialog.getWindow().setBackgroundDrawable(
//                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.about_dialog);
        dialog.show();
    }





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_add_person);
        imgBtn=findViewById(R.id.imageButton);
        addBtn=findViewById(R.id.saveBtn);
        idTxt=findViewById(R.id.editID);
        nameTxt=findViewById(R.id.editName);
        ageTxt=findViewById(R.id.editName);
        genderTxt=findViewById(R.id.editGender);
        raceTxt=findViewById(R.id.editRace);
        eyesTxt=findViewById(R.id.editEyes);
        heightTxt=findViewById(R.id.editHeight);
        weightTxt=findViewById(R.id.editWeight);
        missingDateTxt=findViewById(R.id.editLastTimeSeen);
        missingLocationTxt=findViewById(R.id.editLocation);
        moreIdCharTxt=findViewById(R.id.editIDMoreDet);
        moreDetailsTxt=findViewById(R.id.editMoreDet);
        tel1Txt=findViewById(R.id.editTel1);
        tel2Txt=findViewById(R.id.editTel2);
        imgBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                getPickImageIntent(AddPersonActivity.this);
            }
        });



        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1988);
        cal.set(Calendar.MONTH, Calendar.JANUARY);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        Date dateRepresentation = cal.getTime();
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LostAndFoundData newPerson=new LostAndFoundData
                        (   idTxt.getText().toString()
                            , ageTxt.getText().toString()
                            , ((BitmapDrawable) ResourcesCompat.getDrawable(getResources(), R.drawable.man,null)).getBitmap()
                            , genderTxt.getText().toString()
                            , new Date(missingDateTxt.getText().toString())
                                , moreDetailsTxt.getText().toString()) ;
                db.insertData(FeedEntry.TABLE_LOST,newPerson);
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // R.menu.mymenu is a reference to an xml file named mymenu.xml which should be inside your res/menu directory.
        // If you don't have res/menu, just create a directory named "menu" inside res
        getMenuInflater().inflate(R.menu.menu_with_back, menu);
        return super.onCreateOptionsMenu(menu);
    }
    public void showDatePicker(View v) {
        DialogFragment newFragment = new LastTimeSeenDatePicker();

        newFragment.show(getSupportFragmentManager(), "Date Picker");
    }
    // handle button activities
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.backBtn) {
         this.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
    private void startCameraForResultActivity(Context context){
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED){
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.CAMERA}, CAMERA_REQUEST_CODE);

        }
        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(takePicture, 0);
    }
    private void startStorageForResult(Context context){
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_DENIED){
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_REQUEST_CODE);

        }
        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto , 1);//one can be replaced with any action code
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch(requestCode) {
            case 0:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = imageReturnedIntent.getData();
                    imgBtn.setImageURI(selectedImage);
                }

                break;
            case 1:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = imageReturnedIntent.getData();
                    imgBtn.setImageURI(selectedImage);
                }
                break;
        }
    }
}
