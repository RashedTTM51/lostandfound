package com.LCloud.LostAndFound.ui.notifications;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.LCloud.LostAndFound.DBControl.DbHelper;
import com.LCloud.LostAndFound.DBControl.FeedEntry;
import com.LCloud.LostAndFound.Model.LostAndFoundData;
import com.LCloud.LostAndFound.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class FoundFragment extends Fragment {


    DbHelper db;
    FoundRecyclerViewAdapter adapter;
    ArrayList<LostAndFoundData> dataList;
    RecyclerView recyclerView;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d("got here", "onCreateView: ");
        View root = inflater.inflate(R.layout.fragment_lost, container, false);
        db = new DbHelper(getActivity());
        dataList = new ArrayList<>();
        viewData();
        adapter = new FoundRecyclerViewAdapter(this.getContext(), dataList);
        recyclerView = root.findViewById(R.id.lost_fragment_recycle);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);

        return root;
    }

    private void viewData() {
        dataList = db.viewData(FeedEntry.TABLE_FOUND);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("lost fragment", "onResume:0");
    }
}

class FoundRecyclerViewAdapter extends RecyclerView.Adapter<FoundRecyclerViewAdapter.ViewHolder> {

    private List<LostAndFoundData> mData;
    private LayoutInflater mInflater;
    //  private ItemClickListener mClickListener;
//     private ItemClickListener shareLinkListner;

    // data is passed into the constructor
    FoundRecyclerViewAdapter(Context context, List<LostAndFoundData> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    // inflates the row layout from xml when needed
    @Override
    public FoundRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.content_lost, parent, false);
        return new FoundRecyclerViewAdapter.ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(FoundRecyclerViewAdapter.ViewHolder holder, int position) {

        LostAndFoundData data;
        String id=mData.get(position).getId();
        String idNum=mData.get(position).getIdNum();
        String age=mData.get(position).getAge();
        Bitmap img =mData.get(position).getImg();
        String gender =mData.get(position).getGender();
        String details =mData.get(position).getDetails();
        Date dateTime=mData.get(position).getTimeStampDate();
        Date lastTime=mData.get(position).getLastTimeSeen();
        holder.idTxt.setText(idNum);
        holder.ageTxt.setText(age);
        holder.genderTxt.setText(gender);
        holder.imgView.setImageBitmap(img);
        if(lastTime!=null)
            holder.lastSeenTxt.setText(lastTime.toString());
        else
            holder.lastSeenTxt.setText("try");

    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView idTxt;
        TextView ageTxt;
        TextView genderTxt;
        TextView lastSeenTxt;
        ImageView imgView;


        ViewHolder(View itemView) {
            super(itemView);
            idTxt = itemView.findViewById(R.id.lost_id_num);
            ageTxt = itemView.findViewById(R.id.lost_age);
            genderTxt = itemView.findViewById(R.id.lost_gender);
            imgView = itemView.findViewById(R.id.lost_img);
            lastSeenTxt = itemView.findViewById(R.id.lost_last_seen);
        }

//        @Override
//        public void onClick(View view) {
////            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
////            sharingIntent.setType("text/plain");
////
////
////            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
////            view.getContext().startActivity(Intent.createChooser(sharingIntent, "Share via"));
//            if (mClickListener != null){
//                Intent shareIntent = new Intent();
//                shareIntent.setAction(Intent.ACTION_SEND);
//                shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(path));
//                shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, linkTxt.getText().toString());
//                shareIntent.setType("image/*");
//                view.getContext().startActivity(Intent.createChooser(shareIntent,  "Share via"));
//            }
//            if (mClickListener != null){
//                Intent shareIntent = new Intent();
//                shareIntent.setAction(Intent.ACTION_SEND);
//                shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(path));
//                shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, linkTxt.getText().toString());
//                shareIntent.setType("image/*");
//                view.getContext().startActivity(Intent.createChooser(shareIntent,  "Share via"));
//            }
//        }

        // convenience method for getting data at click position
        LostAndFoundData getItem(int id) {
            return mData.get(id);
        }


        // allows clicks events to be caught
//        void setClickListener(ItemClickListener itemClickListener) {
//            this.mClickListener = itemClickListener;
//
//        }
//        //
////
////
////    // parent activity will implement this method to respond to click events
//        public interface ItemClickListener {
//            void onItemClick(View view, int position);
//            void onDelClick(View view, int position);
//
//        }
    }
}
