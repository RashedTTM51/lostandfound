package com.LCloud.LostAndFound.Model;

import android.graphics.Bitmap;

import com.LCloud.LostAndFound.Util.Utils;

import java.util.Date;

public class LostAndFoundData {
    private  String id;
    private String idNum;
    private String age;
    private Bitmap img;
    private String gender;
    private Date timeStampDate;
    private Date lastTimeSeen;
    private String details;
    private String eyes;
    private String race;
    private String hair;
    private String height;
    private String weight;
    private String other_id_data;
    private String lastLocationSeen;











    public LostAndFoundData(String id, String idNum, String age, Bitmap img, String gender, Date timeStampDate, Date lastTimeSeen, String details) {
        this.id = id;
        this.idNum = idNum;
        this.age = age;
        this.img = img;
        this.gender = gender;
        this.timeStampDate = timeStampDate;
        this.lastTimeSeen = lastTimeSeen;
        this.details = details;
    }

    public LostAndFoundData(String idNum, String age, Bitmap img, String gender, Date lastTimeSeen, String details) {
        this.id= Utils.generateUUID();
        this.idNum = idNum;
        this.age = age;
        this.img = img;
        this.gender = gender;
        this.lastTimeSeen = lastTimeSeen;
        this.details = details;
    }

    public String getId() {
        return id;
    }

    public Bitmap getImg() {
        return img;
    }

    public void setImg(Bitmap img) {
        this.img = img;
    }

    public String getIdNum() {
        return idNum;
    }

    public void setIdNum(String idNum) {
        this.idNum = idNum;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getTimeStampDate() {
        return timeStampDate;
    }

    public void setTimeStampDate(Date timeStampDate) {
        this.timeStampDate = timeStampDate;
    }

    public Date getLastTimeSeen() {
        return lastTimeSeen;
    }

    public void setLastTimeSeen(Date lastTimeSeen) {
        this.lastTimeSeen = lastTimeSeen;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }






}
