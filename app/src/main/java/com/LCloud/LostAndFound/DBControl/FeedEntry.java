package com.LCloud.LostAndFound.DBControl;

import android.provider.BaseColumns;

public  class FeedEntry implements BaseColumns {
    public static final String TABLE_FOUND = "lost_history";
    public static final String COLUMN_FOUND_TIMESTAMP= "time";
    public static final String COLUMN_FOUND_ID= "id";
    public static final String COLUMN_FOUND_ID_NUMBER= "id_num";
    public static final String COLUMN_FOUND_GENDER= "gender";
    public static final String COLUMN_FOUND_RACE= "race";
    public static final String COLUMN_FOUND_EYES= "eyes";
    public static final String COLUMN_FOUND_HAIR= "hair";
    public static final String COLUMN_FOUND_HEIGHT= "height";
    public static final String COLUMN_FOUND_WEIGHT= "weight";
    public static final String COLUMN_FOUND_AGE = "age";
    public static final String COLUMN_FOUND_IMG = "img";
    public static final String COLUMN_FOUND_LAST_TIME_SEEN = "last_time_seen";
    public static final String COLUMN_FOUND_LAST_LOCATION_SEEN= "last_location_seen";
    public static final String COLUMN_FOUND_OTHER_IDENTFCATION= "other_id";

    public static final String COLUMN_FOUND_OTHER = "details";

    public static final String TABLE_LOST = "found_history";
//    public static final String COLUMN_LOST_ID= "id";
//    public static final String COLUMN_LOST_TIMESTAMP= "time";
//    public static final String COLUMN_LOST_TYPE= "type";
//    public static final String COLUMN_LOST_LINK = "data";
//    public static final String COLUMN_LOST_IMG = "img";
//    public static final String COLUMN_LOST_PATH = "path";
//    public static final String COLUMN_LOST_OTHER = "details";


}
