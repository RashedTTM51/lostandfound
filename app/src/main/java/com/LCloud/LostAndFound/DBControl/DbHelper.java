package com.LCloud.LostAndFound.DBControl;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.os.Build;
import android.util.Log;

import com.LCloud.LostAndFound.BuildConfig;
import com.LCloud.LostAndFound.Model.LostAndFoundData;
import com.LCloud.LostAndFound.Util.Constants;
import com.LCloud.LostAndFound.Util.Utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class DbHelper  extends SQLiteOpenHelper {

    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 3;
    public static final String DATABASE_NAME = "LostAndFound.db";


    private static final String SQL_CREATE_ENTRIES_FOUND =
            "CREATE TABLE " + FeedEntry.TABLE_FOUND + " (" +
                    FeedEntry.COLUMN_FOUND_TIMESTAMP + " DATETIME DEFAULT CURRENT_TIMESTAMP ," + FeedEntry.COLUMN_FOUND_GENDER + " TEXT,"+ FeedEntry.COLUMN_FOUND_OTHER + " TEXT,"+ FeedEntry.COLUMN_FOUND_ID_NUMBER + " TEXT,"+ FeedEntry.COLUMN_FOUND_LAST_TIME_SEEN + " DATETIME," + FeedEntry.COLUMN_FOUND_AGE + " TEXT,"+ FeedEntry.COLUMN_FOUND_IMG + " BLOB,"+
                    FeedEntry.COLUMN_FOUND_ID + " TEXT,PRIMARY KEY("+FeedEntry.COLUMN_FOUND_ID+","+FeedEntry.COLUMN_FOUND_TIMESTAMP+"))";
    private static final String SQL_CREATE_ENTRIES_LOST =
            "CREATE TABLE " + FeedEntry.TABLE_LOST + " (" +
                    FeedEntry.COLUMN_FOUND_TIMESTAMP + " DATETIME DEFAULT CURRENT_TIMESTAMP ," + FeedEntry.COLUMN_FOUND_GENDER + " TEXT,"+ FeedEntry.COLUMN_FOUND_OTHER + " TEXT,"+ FeedEntry.COLUMN_FOUND_ID_NUMBER + " TEXT,"+ FeedEntry.COLUMN_FOUND_LAST_TIME_SEEN + " DATETIME," + FeedEntry.COLUMN_FOUND_AGE + " TEXT,"+ FeedEntry.COLUMN_FOUND_IMG + " BLOB,"+
                    FeedEntry.COLUMN_FOUND_ID + " TEXT,PRIMARY KEY("+FeedEntry.COLUMN_FOUND_ID+","+FeedEntry.COLUMN_FOUND_TIMESTAMP+"))";


    private static final String SQL_DELETE_ENTRIES_LOST =
            "DROP TABLE IF EXISTS " + FeedEntry.TABLE_LOST;
    private static final String SQL_DELETE_ENTRIES_FOUND =
            "DROP TABLE IF EXISTS " + FeedEntry.TABLE_FOUND;

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES_FOUND);
        db.execSQL(SQL_CREATE_ENTRIES_LOST);
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_ENTRIES_LOST);
        db.execSQL(SQL_DELETE_ENTRIES_FOUND);
        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
    public boolean insertData(String tableName,String idNum,String age,String gender,Bitmap image,String lastTime,String details ){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contetV= new ContentValues();
        long result=-1;
        String id= Utils.generateUUID();
        if (id!="") {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
             contetV.put(FeedEntry.COLUMN_FOUND_ID, id);
            contetV.put(FeedEntry.COLUMN_FOUND_ID_NUMBER,idNum);
            contetV.put(FeedEntry.COLUMN_FOUND_AGE,age);
            contetV.put(FeedEntry.COLUMN_FOUND_GENDER, gender);
            contetV.put(FeedEntry.COLUMN_FOUND_IMG,Utils.imgToByteArray(image));
         //   contetV.put(FeedEntry.COLUMN_FOUND_LAST_TIME_SEEN,dateFormat.format(lastTime) );
            contetV.put(FeedEntry.COLUMN_FOUND_OTHER, details);
            if(BuildConfig.DEBUG) {
                Log.d(Constants.DATA_BASE_CONTROLER, "insertData: "+contetV.toString());
            }
            result=db.insert(tableName,null,contetV);
        }

        return  result!=-1;

    }
    public boolean insertData(String tableName,LostAndFoundData missingPerson){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contetV= new ContentValues();
        long result=-1;
        String id= Utils.generateUUID();
        if (id!="") {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            contetV.put(FeedEntry.COLUMN_FOUND_ID,missingPerson.getId() );
            contetV.put(FeedEntry.COLUMN_FOUND_ID_NUMBER,missingPerson.getIdNum());
            contetV.put(FeedEntry.COLUMN_FOUND_AGE,missingPerson.getAge());
            contetV.put(FeedEntry.COLUMN_FOUND_GENDER, missingPerson.getGender());
            contetV.put(FeedEntry.COLUMN_FOUND_IMG,Utils.imgToByteArray(missingPerson.getImg()));
            //   contetV.put(FeedEntry.COLUMN_FOUND_LAST_TIME_SEEN,dateFormat.format(lastTime) );
            contetV.put(FeedEntry.COLUMN_FOUND_OTHER, missingPerson.getDetails());
            if(BuildConfig.DEBUG) {
                Log.d(Constants.DATA_BASE_CONTROLER, "insertData: "+contetV.toString());
            }
            result=db.insert(tableName,null,contetV);
        }

        return  result!=-1;

    }
    public boolean delData(String tableName,String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(tableName, FeedEntry.COLUMN_FOUND_ID + " = " + id , null) > 0;
    }

    public Cursor getViewData(String tableName){
        SQLiteDatabase db= this.getReadableDatabase();
        String query = "Select * from "+ tableName;
        Cursor cursor = db.rawQuery(query,null);
        return  cursor;
    }
    public ArrayList<LostAndFoundData> viewData(String tableName){
        Date dateTime=null;
        Date lastTime=null;
        LostAndFoundData data;
        String id;
        String idNum;
        String age;
        Bitmap img ;
        String gender ;
        String details ;
        ArrayList<LostAndFoundData> dataList=new ArrayList<>();
        Cursor cursor = getViewData(FeedEntry.TABLE_LOST);
        if(cursor!=null && cursor.getCount()>0){
            while(cursor.moveToNext()){
                String dateTimeStamp = cursor.getString(cursor.getColumnIndexOrThrow(FeedEntry.COLUMN_FOUND_TIMESTAMP));
                DateFormat iso8601Format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                try {
                    dateTime = iso8601Format.parse(dateTimeStamp);
                } catch (ParseException e) {
                    Log.e("db get time", "Parsing ISO8601 datetime failed", e);
                }
                String lastSeenDate = cursor.getString(cursor.getColumnIndexOrThrow(FeedEntry.COLUMN_FOUND_LAST_TIME_SEEN));
                try {
                    if(lastSeenDate!=null)
                    lastTime = iso8601Format.parse(lastSeenDate);
                } catch (ParseException e) {
                    Log.e("db get time", "Parsing ISO8601 datetime failed", e);
                }
                id=cursor.getString(cursor.getColumnIndexOrThrow(FeedEntry.COLUMN_FOUND_ID));
                idNum=cursor.getString(cursor.getColumnIndexOrThrow(FeedEntry.COLUMN_FOUND_ID_NUMBER));
                age=cursor.getString(cursor.getColumnIndexOrThrow(FeedEntry.COLUMN_FOUND_AGE));
                img =Utils.byteArraytoImg(cursor.getBlob(cursor.getColumnIndexOrThrow(FeedEntry.COLUMN_FOUND_IMG)));
                gender =cursor.getString(cursor.getColumnIndexOrThrow(FeedEntry.COLUMN_FOUND_GENDER));
                details =cursor.getString(cursor.getColumnIndexOrThrow(FeedEntry.COLUMN_FOUND_OTHER));
                data= new LostAndFoundData(id,idNum, age, img, gender,dateTime, lastTime,  details) ;
                if(dateTime!=null){

                     dataList.add(data);
                    }
                    else {

                        dataList.add(data);
                    }


                }

            }
        return dataList;
        }



}
