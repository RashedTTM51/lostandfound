package com.LCloud.LostAndFound.Util;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class PremissionCheck {
    static public boolean checkPermission(Context ctx,String PrimType) {
        return ( ContextCompat.checkSelfPermission(ctx, PrimType ) == PackageManager.PERMISSION_GRANTED);
    }

    static public void requestPermission(Activity act, String PrimType,int reqCode) {
        ActivityCompat.requestPermissions(act, new String[]{PrimType}, reqCode);
    }

}
